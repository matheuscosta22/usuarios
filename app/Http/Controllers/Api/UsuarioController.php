<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Models\Usuario;
use Illuminate\Http\Request;

class UsuarioController extends Controller
{
    
    // Lista todos os usuários
    public function list(){
        $usuario = Usuario::all();

        return $usuario;
    }

    // Adiciona um usuário
    public function add(Request $request){
        try{
            $usuario = new Usuario();
            $usuario->nome = $request->nome;
            $usuario->telefone = $request->telefone;
            $usuario->cpfCnpj = $request->cpfCnpj;
            $usuario->dataDeNascimento = $request->dataDeNascimento;
            $usuario->senha = $request->senha;

            $usuario->save();
            return $usuario;

        }catch(\Exception $erro){
            return $erro;
        }
    }

    // Atualiza o usuário selecionado
    public function update(Request $request, $id){
        try{
            $usuario = Usuario::find($id);
            $usuario->nome = $request->nome;
            $usuario->telefone = $request->telefone;
            $usuario->cpfCnpj = $request->cpfCnpj;
            $usuario->dataDeNascimento = $request->dataDeNascimento;
            $usuario->senha = $request->senha;

            $usuario->save();
            return ['updated successfully!', $usuario];
        }catch(\Exception $erro){
            return $erro;
        }
    }

    // Deleta o usuário selecionado
    public function delete($id){
        try{

            $usuario = Usuario::find($id);
            $usuario->delete();
            return ['successfully deleted'];
        }catch(\Exception $erro){
            return $erro;
        }
    }
}
