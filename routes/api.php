<?php

use Faker\Guesser\Name;
use Illuminate\Http\Request;
use Illuminate\Routing\RouteRegistrar;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});
//------------------------------------------------------------------------------

    //Rotas Api
Route::namespace('Api')->group( function(){
    Route::post('/usuario/criar', [App\Http\Controllers\Api\UsuarioController::class, 'add'])->name('/usuario/criar');
    Route::get('/usuarios', [App\Http\Controllers\Api\UsuarioController::class, 'list'])->name('/usuarios');
    Route::put('/usuario/{id}', [App\Http\Controllers\Api\UsuarioController::class, 'update'])->name('/usuario/{id}');
    Route::delete('/usuario/{id}', [App\Http\Controllers\Api\UsuarioController::class, 'delete'])->name('/usuario/{id}');
});